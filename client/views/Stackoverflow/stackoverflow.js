
$("#menu-toggle").click(function(e) {
    e.preventDefault();
    console.log("toggle");
    $("#wrapper").toggleClass("active");
});


Template.home.helpers({
   'init':function()
   {
     
     
   }
});

Template.home.events
({
    
    'click .button':function(e)
    {
       var doc = new jsPDF();
         var source = $('#stackoverflow').first();
         var specialElementHandlers = {
             
         };

         doc.fromHTML(
             source, // HTML string or DOM elem ref.
             0.5,    // x coord
             0.5,    // y coord
             {
                 'width': 7.5, // max width of content on PDF
                 'elementHandlers': specialElementHandlers
             });

         doc.output('dataurlnewwindow');  
        // doc.save('test.pdf');
    },
   'submit #stackoverflowID':function(e)
   {
     e.preventDefault();
    var surl=$('#stackurl').val();
    if(surl ==="" || isNaN(surl))
    {
        alert("ID must be a number");
    }
    else
    {
       
      //User Basic Information
      var userDetailsURL = "http://api.stackexchange.com/2.2/users/"+surl;
      HTTP.call("GET", userDetailsURL,{params:{site:"stackoverflow",key:'0DIk0wZkEh9G1uo*R3fvNg(('}},function(error,result)
      {
            if(error)
            {
               alert("No such user found");
            }
            else
            {
               if(result.data.items.length!==0)
               {                   
                    basicInformation(result);
               }
               else
               {
                  alert("No such user found");
               }
            }
       });
       
       /*Meteor.call("stackBasic",surl,function(error,result){
           if(result.data.items.length!==0)
               {                   
                    basicInformation(result);
               }
               else
               {
                  alert("No such user found");
               }
       });*/
       
      
       
       //User questions Information
       var questionsURL = "http://api.stackexchange.com/2.2/users/"+surl+"/questions";
       HTTP.call("GET",questionsURL,{params:{site:"stackoverflow",key:'0DIk0wZkEh9G1uo*R3fvNg(('}},function(e,res)
       {
          if(e)
          {
             console.log("error"+e);
          }
          else
          {
            $('#noOfQuestions').html(res.data.items.length);
            mostQuesTechnology(res);
            mostViewedQuestions(res);
          }
        });
        
        var answersURL = "http://api.stackexchange.com/2.2/users/"+surl+"/answers";
        HTTP.call("GET",answersURL,{params:{site:"stackoverflow",key:'0DIk0wZkEh9G1uo*R3fvNg(('}},function(e,r)
        {
          if(e)
          {
             console.log("error"+e);
          }
          else
          {
            $('#noOfAnswers').html(r.data.items.length);
            mostAnswersByTechnology(r);
            acceptedAnswers(r);
            mostUpvotedAnswers(r);
          }
        });
        
    }      
  }
});

function basicInformation(result)
{
    /*$('#cdate').html(result.data.items[0].creation_date);
    
    $('#rcw').html(result.data.items[0].reputation_change_week);
    $('#rcm').html(result.data.items[0].reputation_change_month);
    $('#rcy').html(result.data.items[0].reputation_change_year);
    $('#gld').html(result.data.items[0].badge_counts.gold);
    $('#slr').html(result.data.items[0].badge_counts.silver);
    $('#bnz').html(result.data.items[0].badge_counts.bronze);*/
    $('#pimg').attr('src',result.data.items[0].profile_image);
    $('#arate').html(result.data.items[0].accept_rate);
    $('#loc').html(result.data.items[0].location);
    $('#rep').html(result.data.items[0].reputation);
    $('#uname').html(result.data.items[0].display_name);
    $('#webSite').html(result.data.items[0].website_url);
    $('#webSite').attr('href',result.data.items[0].website_url);
    /*$('.progress').html('');
    if(result.data.items[0].accept_rate < 50)
    {
        $('.progress').append("<div class='progress-bar progress-bar-danger' role='progressbar' aria-valuenow='"+result.data.items[0].accept_rate+"' aria-valuemin='0' aria-valuemax='100' style='width:"+result.data.items[0].accept_rate+"%;'>"+result.data.items[0].accept_rate+"</div>");    
    }
    else if(result.data.items[0].accept_rate >= 50 && result.data.items[0].accept_rate < 75)
    {
        $('.progress').append("<div class='progress-bar progress-bar-warning' role='progressbar' aria-valuenow='"+result.data.items[0].accept_rate+"' aria-valuemin='0' aria-valuemax='100' style='width:"+result.data.items[0].accept_rate+"%;'>"+result.data.items[0].accept_rate+"</div>");    
    }
    else if(result.data.items[0].accept_rate >= 75)
    {
        $('.progress').append("<div class='progress-bar progress-bar-success' role='progressbar' aria-valuenow='"+result.data.items[0].accept_rate+"' aria-valuemin='0' aria-valuemax='100' style='width:"+result.data.items[0].accept_rate+"%;'>"+result.data.items[0].accept_rate+"</div>");    
    }*/
    
}

function mostQuesTechnology(r)
{
    window.tagNamesArray=[];
    for(i=0;i<r.data.items.length;i++)
    {
        for(j=0;j<r.data.items[i].tags.length;j++)
        {                    
            window.tagNamesArray.push(r.data.items[i].tags[j]);                     
        }
    }
    var uniqueTags=[];
    window.data = [];
    $.each(window.tagNamesArray, function(i, el){
        if($.inArray(el, uniqueTags) === -1) uniqueTags.push(el);
    });
           
    for(var k=0;k<uniqueTags.length;k++)
    {
        var tagCount=0;
        for(var l=0;l<window.tagNamesArray.length;l++)
        {
            if(window.tagNamesArray[l]===uniqueTags[k])
            {
                tagCount++;
            }
        }
        window.data[k] = [];   
        window.data[k].push(uniqueTags[k]);
        window.data[k].push(tagCount);
    }
            
    window.data=window.data.slice(0,5);
    window.data.sort(function(a, b) { 
        return a[1] < b[1]?1:-1;
    });

    $('#mostQuesTechnology').highcharts({
        chart: {
            // plotBackgroundColor: null,
            backgroundColor: null,
            plotShadow: false,
            
        },
        exporting: {
         enabled: false
        },
        credits: false,
        title: {
             text: null
        },
        tooltip: {
            pointFormat: '{point.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    // format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            type: 'pie',
            name: 'Browser share',
            data: window.data
        }]
    });
}


function mostViewedQuestions(res)
{
    $('#questionViews').html('');
    window.viewCount=[];
    var tenk=0;
    var fivek=0;
    var onek=0;
    var fiveh=0;
    var hun=0;
    for(var ques=0;ques<res.data.items.length;ques++)
    {
        window.viewCount.push(res.data.items[ques].view_count);
    }
   
    for(var x=0;x<window.viewCount.length;x++)
    {
        if(window.viewCount[x]>= 10000)
        {
            tenk++;
        }
        else if(window.viewCount[x]< 10000 && window.viewCount[x] >= 5000)
        {
            fivek++;
        }
        else if(window.viewCount[x]< 5000 && window.viewCount[x] >= 1000)
        {
            onek++;
        }
        else if(window.viewCount[x]< 1000 && window.viewCount[x] >= 500)
        {
            fiveh++;
        }
        else if(window.viewCount[x]< 500 && window.viewCount[x] >= 100)
        {
            hun++;
        }
        else
        {

        }
    }
    var s1 = [tenk, fivek, onek, fiveh,hun];
    var ticks = ['10k+ Views', '5k+ Views ', '1k+ Views', '500+ Views','100+ Views'];
    $('#questionViews').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: null
        },
        exporting: {
         enabled: false
        },
        credits: false,
        xAxis: {
            categories: ticks
        },
        yAxis: {
            min: 0
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{name:'Views',data:s1}]
    });
}

function mostAnswersByTechnology(r)
{
    window.question_ids=[];
    window.question_ids_tags=[];
    var ids_string;
    
    for(var y=0;y<r.data.items.length;y++)
    {
        //console.log(r.data.items[y].question_id);
        window.question_ids.push(r.data.items[y].question_id);
    }
    ids_string=window.question_ids[0];
    for(var z=1;z<window.question_ids.length;z++)
    {
        ids_string=ids_string+";"+window.question_ids[z];
    }
    
    var questionIdURL = "http://api.stackexchange.com/2.2/questions/"+ids_string;
    HTTP.call("GET",questionIdURL,{params:{site:"stackoverflow",key:'0DIk0wZkEh9G1uo*R3fvNg(('}},function(e,dat)
    {
      if(e)
      {
         console.log("error"+e);
      }
      else
      {
        for(var n=0;n<dat.data.items.length;n++)
        {
            for(var h=0;h<dat.data.items[n].tags.length;h++)
            {
                window.question_ids_tags.push(dat.data.items[n].tags[h]);    
            }
        }
        drwaAnswerTechnologyChart();
      }
      
    });
}

function drwaAnswerTechnologyChart()
{
    var uniqueAnswerTags=[];
    window.answerTagData = [];
    $.each(window.question_ids_tags, function(i, el){
        if($.inArray(el, uniqueAnswerTags) === -1) uniqueAnswerTags.push(el);
    });
     for(var k=0;k<uniqueAnswerTags.length;k++)
    {
        var tagCount=0;
        for(var l=0;l<window.question_ids_tags.length;l++)
        {
            if(window.question_ids_tags[l]===uniqueAnswerTags[k])
            {
                tagCount++;
            }
        }
        window.answerTagData[k] = [];
        //var slString=uniqueAnswerTags[k].slice(0,14);
        window.answerTagData[k].push(uniqueAnswerTags[k]);
        window.answerTagData[k].push(tagCount);
    }
    window.answerTagData.sort(function(a, b) { 
        return a[1] < b[1]?1:-1;
    });
    window.answerTagData=window.answerTagData.slice(0,5);
    $('#ansByTechnology').highcharts({
        chart: {
            // plotBackgroundColor: null,
            backgroundColor: null,
            plotShadow: false,
            
        },
        credits: false,
        title: {
            text: null
        },
        exporting: {
         enabled: false
        },
        tooltip: {
            pointFormat: '{point.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    // format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            type: 'pie',
            name: 'Question percentage',
            data: window.answerTagData
        }]
    });
}

function acceptedAnswers(r)
{
    var acceptCount=0;
    for(var j=0;j<r.data.items.length;j++)
    {
        if(r.data.items[j].is_accepted)
        {
            acceptCount++;
        }
    }
    $('#noOfAcceptedAnswers').html(acceptCount);
    var acceptPercentage=Math.floor((acceptCount/r.data.items.length)*100);
    $('#bigfella').html("");
    var gaugeOptions = {

        chart: {
            type: 'solidgauge'
        },

        title: null,

        pane: {
            center: ['50%', '70%'],
            size: '80%',
            startAngle: -90,
            endAngle: 90,
            background: {
                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
                innerRadius: '60%',
                outerRadius: '100%',
                shape: 'arc'
            }
        },

        tooltip: {
            enabled: false
        },

        // the value axis
        yAxis: {
            stops: [
                [0.1, '#DF5353'], // green
                [0.5, '#DDDF0D'], // yellow
                [0.9, '#55BF3B'] // red
            ],
            lineWidth: 0,
            minorTickInterval: null,
            tickPixelInterval: 400,
            tickWidth: 0,
            title: {
                y: -70
            },
            labels: {
                y: 16
            }
        },

        plotOptions: {
            solidgauge: {
                dataLabels: {
                    y: 5,
                    borderWidth: 0,
                    useHTML: true
                }
            }
        }
    };

    // The speed gauge
    $('#bigfella').highcharts(Highcharts.merge(gaugeOptions, {
        yAxis: {
            min: 0,
            max: 100,
            title: {
                // text: 'Accept Rate'
            }
        },
        exporting: {
         enabled: false
        },
        credits: {
            enabled: false
        },

        series: [{
            name: 'Accept Rate',
            data: [acceptPercentage],
            dataLabels: {
                format: '<div style="text-align:center"><span style="font-size:25px;color:' +
                    ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}%</span><br/>' +
                       '<span style="font-size:12px;color:silver">\</span></div>'
            },
            tooltip: {
                valueSuffix: ' Accept Rate'
            }
        }]

    }));
}

function mostUpvotedAnswers(r)
{
    $('#mostUpvoteAns').html("");
    var upScores=[];
    for(var i=0;i<r.data.items.length;i++)
    {
        upScores.push(r.data.items[i].score);
    }
    upScores.sort(function(a, b) { 
        return b-a;
    });
    upScores=upScores.slice(0,5);
    var ticks = ['1', '2 ', '3', '4','5'];
    $('#mostUpvoteAns').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: null
        },
        credits: false,
        xAxis: {
            categories: upScores
        },
        exporting: {
         enabled: false
        },
        yAxis: {
            min: 0,
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{name:'Views',data:upScores}]
    });
}