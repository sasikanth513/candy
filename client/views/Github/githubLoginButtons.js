Template.githubLoginButtons.events({
    'submit #githubLoginForm':function(e){
        e.preventDefault();
        var gituname=$('#githubUsername').val();
        var gpassword=$('#githubPassword').val();
        if(gituname==="" || gpassword==="")
        {
            alert("Username and Password must not be empty");
        }
        else
        {
            //To get the basic details of a authenticated user
            Meteor.call("githubBasicDetails",gituname, gpassword, function(error,r){
                if(error)
                {
                    alert("User not found"+error);
                }
                else
                {
                    Session.set("gitUName",gituname);
                    $("#noOfPublicRepos").html(r.result.public_repos);
                    $("#noOfPrivateRepos").html(r.result.owned_private_repos);
                    $("#followers").html(r.result.followers);
                    $("#following").html(r.result.following);
                    $("#githubImage").attr('src',r.result.avatar_url);
                    $("#location").html(r.result.location);
                    $("#hireable").html(r.result.hireable);
                }
            });
            
            //To get the repos of a authenticated user
            Meteor.call("userRepos",function(error,res){
               console.log(res) ;
               for(var i=0;i<res.result.length;i++)
               {
                  if(res.result[i].fork === false && res.result[i].permissions.admin === true)
                    // if(res.result[i].fork === false)
                   {
                       $(".user-repos").append("<li id='"+res.result[i].name+"'><a href=''>"+res.result[i].name+"</a></li>");
                   }
               }
            });
            
            /*Meteor.call("userOrgs",function(error,res){
                console.log(res.result[1].login);
                console.log(res);
                Meteor.call("orgRepos",res.result[1].login,function(e,r){
                    console.log(r);
                });
            });*/
        }
    }
});
