
Meteor.startup(function(){
    google.setOnLoadCallback(drawChart);
});
var additions=0;
var deletions=0;
var dupArray=[0,1];
var addArray=[0];
var dupTicks=[1,2];
window.ticks=["start date"];

Template.githubUserRepos.events({
    'click .user-repos li':function(e){
        
        additions=0;
        deletions=0;
        addArray=[0];
        window.ticks=["start"];
        window.line=[];

        var repoName=e.currentTarget.id;
        $("#repoName").html(repoName);
        var uname=Session.get("gitUName");
        /*Meteor.call("repocontent",uname,repoName,function(e,r){
          console.log("repo data files");
          console.log(r);
        })*/
        Meteor.call("repoData",uname,repoName,function(e,repodata){
            console.log(repodata);
           $("#noOfForks").html(repodata.result.forks);
           $("#noOfStars").html(repodata.result.stargazers_count);
           $("#noOfSubscribes").html(repodata.result.subscribers_count);
        });
        Meteor.call("repoCommits",uname,repoName,function(e,r){
            console.log(r);
            // window.commitsLength=r.result.length;
            for(var s=0;s<r.result.length;s++)   
            {
               var commitDate=new Date(r.result[s].commit.author.date);
               var month=commitDate.getMonth()+1;
               var dat=commitDate.getDate()+"."+month+"."+commitDate.getFullYear();
               var shda=commitDate.getDate()+"."+month;
               
               if(window.line.length===0)
               {
                    window.line[0]=[];
                    window.line[0].push(dat);
                    window.line[0].push(1);
                    
                    // window.line[0].push(shda);
               }
               else
               {
                   var re=hasDate(dat);
                   if(!re)
                   {
                       
                       var x=window.line.length;
                       window.line[x]=[];
                        window.line[x].push(dat);
                        window.line[x].push(1);
                        
                        // window.line[x].push(shda);
                   }
               }
               
            
               Meteor.call("stats",uname,repoName,r.result[s].sha,function(err,res){
                    
                //   console.log(res.result.commit.author.date);
                   var ss=new Date(res.result.commit.author.date);
                   var month=ss.getMonth()+1;
                   var da=ss.getDate()+"/"+month;
                   window.ticks.push(da);
                //   console.log(da);
                    additions=additions+res.result.stats.additions;
                    deletions=deletions+res.result.stats.deletions;
                    addArray.push(res.result.stats.additions);
                    $('#additions').html(additions);
                    $('#deletions').html(deletions);
                    var chart = $('#chart1b').highcharts();
                    chart.series[0].setData(addArray);
                    chart.xAxis[0].setCategories(window.ticks);
                    // window.options.axes.xaxis.ticks=window.ticks;
                });
            }
            $("#lineChart").html(" ");
            
            window.ddata.removeRows(0,window.ddata.getNumberOfRows());
            window.ddata.addRows(window.line);
            var chart = new google.visualization.LineChart(document.getElementById('lineChart'));
            chart.draw(window.ddata, window.ooptions);
            /*$('#lineChart').highcharts({
                title: {
                    text: null
                },
                exporting:{
                  enabled:false
                },
                credits: false,
                xAxis: {
                    categories: dupTicks
                },
                yAxis: {
                    title: {
                        text: null,
                    },
                    min: 0,
                    plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
                },
                series: [{name:'Commits By Date',data:window.ddata}]
            });*/
              
       });
       

         console.log("duplicate adding");
         console.log(dupArray);
        // var plot1 = $.jqplot('chart1b',[dupArray],window.options);
             $('#chart1b').highcharts({
                title: {
                    text: null
                },
                exporting:{
                  enabled:false
                },
                credits: false,
                xAxis: {
                    categories: dupTicks
                },
                yAxis: {
                    title: {
                        text: null,
                    },
                    min: 0,
                    plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
                },
                series: [{name:'Number of Lines',data:dupArray}]
            });
          
            window.gitLangData=[];
           
            Meteor.call("repoLanguage",uname,repoName,function(e,r){
                var k=0;
                if (!r.error) {
                    $.each(r.result, function(key, data) {
                        if (key != 'meta') {
                                console.log(key + ' = ' + data);
                                window.gitLangData[k]=[];
                                window.gitLangData[k].push(key);
                                window.gitLangData[k].push(data);
                                k++;
                        }
                    });
                }
                
                if(window.gitLangData.length===0)
                {
                    window.gitLangData=[["no language",1]];
                }
                $('#pieChart').highcharts({
                  chart: {
                      // plotBackgroundColor: null,
                      backgroundColor: null,
                      plotShadow: false,
                      
                  },
                  exporting: {
                   enabled: false
                  },
                  credits: false,
                  title: {
                       text: null
                  },
                  tooltip: {
                      pointFormat: '{point.name}: <b>{point.percentage:.1f}%</b>'
                  },
                  plotOptions: {
                      pie: {
                          allowPointSelect: true,
                          cursor: 'pointer',
                          dataLabels: {
                              enabled: true,
                              // format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                              style: {
                                  color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                              }
                          }
                      }
                  },
                  series: [{
                      type: 'pie',
                      name: 'Browser share',
                      data: window.gitLangData
                  }]
              });
            });
            
            Meteor.call("gists",function(e,d){
                Session.set('gists',d);
               getGistData();
            });    
               
           
       }     
 

});

function getGistData()
{
    window.gistData=[];
    var sa=0;
    
    var d=Session.get('gists');
    
    _.each(d.result,function(dd){
           Meteor.call("gist",dd.id,function(err,res){
              window.gistData[sa]=[];
              window.gistData[sa].push(res.result.description);
              window.gistData[sa].push(res.result.forks.length);
              window.gistData[sa].push(res.result.html_url);
              sa++;
              if(sa===d.result.length)
              {
                    
                    window.gistData.sort(function(a, b) { 
                        return a[1] < b[1]?1:-1;
                    });
                    window.gistData=window.gistData.slice(0,5);
                    _.each(window.gistData,function(da){
                        $("#gists") .append("<li><h4>"+da[0]+"</h4></li><h5><a target='_blank' href="+da[2]+">"+da[2]+"</a></h5>");
                    });
              }
           }); 
    });
    
}

function hasDate(val)
{
   for(var v=0;v<window.line.length;v++)
    {
        
        if(window.line[v][0]===val)
       {
           
           window.line[v][1]=window.line[v][1]+1;
           return true;
       }
       else
       {
           if(v===window.line.length-1)
           {
            return false;    
           }
           
       }
    }
}

function drawChart() {
    console.log("function called");
    
        window.ddata = new google.visualization.DataTable();
        window.ddata.addColumn('string', 'Date');
        window.ddata.addColumn('number', 'Commits');
        window.ooptions = {
        //   title: 'Company Performance'
        };

        
      }