
Template.linkedIn.events({
    'click #getLinkedInData':function(){
        
        
        Meteor.call("linkedInapi",function(e,res){
                console.log(res);

                var name=res.data.firstName+" "+res.data.lastName;

                $("#linkedinName").html(name);
                $("#linkedinStatus").html(res.data.headline);
                $("#linkedinLocation").html(res.data.location.name);
                $("#linkedinTotalConnections").html(res.data.numConnections);
                $("#linkedinImage").attr("src",res.data.pictureUrl);
                $("#linkedinSummary").html(res.data.summary);
                $("#linkedinRecommendors").html(res.data.numRecommenders);
                var endDate="";
                
                var firstName=res.data.firstName;
                if(res.data.positions._total>0)
                {
                    for(var i=0;i<res.data.positions._total;i++)
                    {
                        var compCount=0;
                        var startDate=res.data.positions.values[i].startDate.month+"/"+res.data.positions.values[i].startDate.year;
                        if(res.data.positions.values[i].isCurrent)
                        {
                            $("#linkedinCurrentWorking").html(res.data.positions.values[i].company.name); 
                            endDate="present";
                        }
                        else
                        {
                            endDate=res.data.positions.values[i].endDate.month+"/"+res.data.positions.values[i].endDate.year;
                        }
                        var compName=res.data.positions.values[i].company.name;
                        var compTitle=res.data.positions.values[i].title;
                        var compDur=startDate+" - "+endDate;
                        var compPosId=res.data.positions.values[i].id;
                        var dupCompPosId=compPosId+1;
                        var modalBodyId=compPosId+2;
                        var listDisplayId=compPosId+3;
                        // var expTemplate='<div class="row"><div class="col-md-2"></div><div class="col-md-5"><h4><strong>'+compTitle+'</strong></h4><h4>'+compName+'</h4><h5>'+compDur+'</h5></div><div class="col-md-5"></div></div>';
                        var expTemplate='<div class="row"><div class="col-md-2"></div><div class="col-md-5"><h4><strong>'+compTitle+'</strong></h4><h4>'+compName+'</h4><h5>'+compDur+'</h5></div><div class="col-md-5"><h5> '+firstName+ ' has <span id="'+compPosId+'"></span> Connections from this Company. </h5><ul class="compConnections" id="'+listDisplayId+'"></ul><h5 align="right" style="margin-right:5em"><a href="" data-toggle="modal" data-target="#'+dupCompPosId+'">View All Connections</a></h5></div></div><br/>';
                        var modalTemplate='<div class="modal fade bs-example-modal-sm" id="'+dupCompPosId+'" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true"><div class="modal-dialog modal-sm"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><h4 class="modal-title" id="myModalLabel">All Connections</h4></div><div class="modal-body" id="'+modalBodyId+'"></div><div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Close</button></div></div></div></div>';
                        $("#exp").append(expTemplate);
                        $("#expModals").append(modalTemplate);
                        
                         _.each(res.data.connections.values,function(val){
                            _.each(val.positions.values,function(val2){
                                var connComp=val2.company.name;
                                if(compName===connComp)
                                {
                                    $("#"+modalBodyId).append('<h5><img class="img-circle" height="30px" width="30px" src="'+val.pictureUrl+'"><strong>&nbsp;&nbsp;&nbsp;&nbsp;<a target="_blank" href="'+val.publicProfileUrl+'">'+val.firstName+' '+val.lastName+'</a></strong></h5>');
                                    compCount++;
                                    if(compCount<=2)
                                    {
                                        $("#"+listDisplayId).append('<li><h5><img class="img-circle" height="30px" width="30px" src="'+val.pictureUrl+'"><strong>&nbsp;&nbsp;&nbsp;&nbsp;<a target="_blank" href="'+val.publicProfileUrl+'">'+val.firstName+' '+val.lastName+'</a></strong></h5></li>');
                                    }
                                }
                            });
                            
                        });
                        if(compCount===0)
                        {
                            $("#"+listDisplayId).append('<li><h5><strong>No connections</strong></h5></li>');
                        }
                        $('#'+compPosId).html(compCount);
                    }
                    
                    
                }
                else
                {
                    $("#linkedinCurrentWorking").html("no current status");
                }
                
                var skillRowCount=0;
                var skillColumnCount=0;
                _.each(res.data.skills.values,function(skill){
                   
                   $("#skillChart").append('<h4><span class="label label-success">'+skill.skill.name+'</span></h4>');
                });
                
                var compFollowCount=0;
                var randomNum=Math.floor((Math.random() * 1000000) + 1);
                var randomNum2=Math.floor((Math.random() * 100000000) + 1);
                
                $(".compFollowing").html('<h5 align="right" style="margin-right:5em"><a href="" data-toggle="modal" data-target="#'+randomNum+'">View All Companies</a></h5>');
                var modalCompFollowTemplate='<div class="modal fade bs-example-modal-sm" id="'+randomNum+'" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true"><div class="modal-dialog modal-sm"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><h4 class="modal-title" id="myModalLabel">Following Companies</h4></div><div class="modal-body" id="'+randomNum2+'"></div><div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Close</button></div></div></div></div>';
                $(".compFollowing").append(modalCompFollowTemplate);
                _.each(res.data.following.companies.values,function(comp){
                    Meteor.call("linkedinCompany",comp.id,function(e,d){
                        compFollowCount++;
                        if(compFollowCount<=8)
                        {
                            if(compFollowCount>4)
                            {
                                var newRowTemplate='<div class="col-md-3"><img class="img" width="150px" height="100px" src="'+d.data.logoUrl+'" /><h5><strong>'+comp.name+'</strong></h5><h5>'+d.data.industries.values[0].name+'</h5></div>';
                                $(".followingCompaniesSecond").append(newRowTemplate);    
                            }
                            else
                            {
                                var followingCompanyTemplate='<div class="col-md-3"><img class="img" width="150px" height="100px" src="'+d.data.logoUrl+'" /><h5><strong>'+comp.name+'</strong></h5><h5>'+d.data.industries.values[0].name+'</h5></div>';
                                $(".followingCompaniesFirst").append(followingCompanyTemplate);    
                            }   
                            var nnn='<h5><img class="img-circle" height="30px" width="30px" src="'+d.data.logoUrl+'"><strong>&nbsp;&nbsp;&nbsp;&nbsp;'+d.data.name+'</strong></h5>';
                            $("#"+randomNum2).append(nnn);
                        }
                    });
                });
                
                if(res.data.following.people._total>0)
                {
                    var PeopleFollowCount=0;
                    var peopleRandomNum=Math.floor((Math.random() * 10000000) + 1);
                    var peopleRandomNum2=Math.floor((Math.random() * 1000000000) + 1);
                    
                    $(".peopleFollowing").html('<h5 align="right" style="margin-right:5em"><a href="" data-toggle="modal" data-target="#'+peopleRandomNum+'">View All Companies</a></h5>');
                    var modalPeopleFollowTemplate='<div class="modal fade bs-example-modal-sm" id="'+peopleRandomNum+'" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true"><div class="modal-dialog modal-sm"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><h4 class="modal-title" id="myModalLabel">Following Companies</h4></div><div class="modal-body" id="'+peopleRandomNum2+'"></div><div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Close</button></div></div></div></div>';
                    $(".peopleFollowing").append(modalCompFollowTemplate);
                    _.each(res.data.following.people.values,function(comp){
                        Meteor.call("linkedinCompany",comp.id,function(e,d){
                            PeopleFollowCount++;
                            if(PeopleFollowCount<=8)
                            {
                                if(PeopleFollowCount>4)
                                {
                                    var newRowTemplate='<div class="col-md-3"><img class="img" width="150px" height="100px" src="'+d.data.logoUrl+'" /><h5><strong>'+comp.name+'</strong></h5><h5>'+d.data.industries.values[0].name+'</h5></div>';
                                    $(".followingPeopleSecond").append(newRowTemplate);    
                                }
                                else
                                {
                                    var followingCompanyTemplate='<div class="col-md-3"><img class="img" width="150px" height="100px" src="'+d.data.logoUrl+'" /><h5><strong>'+comp.name+'</strong></h5><h5>'+d.data.industries.values[0].name+'</h5></div>';
                                    $(".followingPeopleFirst").append(followingCompanyTemplate);    
                                }   
                                var nnn='<h5><img class="img-circle" height="30px" width="30px" src="'+d.data.logoUrl+'"><strong>&nbsp;&nbsp;&nbsp;&nbsp;'+d.data.name+'</strong></h5>';
                                $("#"+peopleRandomNum2).append(nnn);
                            }
                        });
                    });
                }
                else
                {
                    $(".peopleFollowing").append("<h4 align='center'>No Following People</h4>");
                }
        });
        
        /*Meteor.call("linkedInapiConnections",function(e,res){
                console.log(res);
                
        });*/
        
    }
});

Template.linkedIn.rendered=function(){
    
    /* var plot2 = $.jqplot('skillChart', [
        [[30,"Data Warehosuing"], [25,"Business Developemnt"], [20,"Business Strategy"], [18,"PHP"],[15,"MY SQL"],[10,"COBOL"],]], {
        seriesDefaults: {
            renderer:$.jqplot.BarRenderer,
            // Show point labels to the right ('e'ast) of each bar.
            // edgeTolerance of -15 allows labels flow outside the grid
            // up to 15 pixels.  If they flow out more than that, they 
            // will be hidden.
            pointLabels: { show: true, location: 'e', edgeTolerance: -15 },
            // Rotate the bar shadow as if bar is lit from top right.
            shadowAngle: 135,
            // Here's where we tell the chart it is oriented horizontally.
            rendererOptions: {
                barDirection: 'horizontal'
            }
        },
        axes: {  
            xaxis:{
                 formatstring: "$%d"
            },
            yaxis: {
                renderer: $.jqplot.CategoryAxisRenderer
            }
        }
    });*/
};
    