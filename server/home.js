Meteor.publish("userData",function(){
    return Meteor.users.find({_id:this.userId},{fields:{"services":1}});
});

var GithubApi = Meteor.npmRequire('github');
  var github = new GithubApi({
      version: "3.0.0"
});
  
var azadtoken="AQUCSVpnDCYOfXG4WR33JN9DZY8C8Fw5Oph5fEd5xmuIPxFVy4KpDLBEzcp38jqel2Vx5SCAbaLYUMv_9BFkZABDibsXSCm9VpEG_XeTRAbaA6Q9MvMG9l1iBqzXhoOsoZlJ7YCMD1Aziaa84kz6dkW0mAar2iGo0ywerYzZVtcl0vAHvRY";
var sasitoken="AQV3a0BjXejQSgELJh4-cppU0bdbKJ6wuEJJ6d1OvuJLvp1Vb14PbY54B3qyYZpow0S6QJIA5xp7CskRCJ4nIVGK5hkXpGbdo7eQOWOtNuGgemPcvE14kuyfRnO4g2UJcGb7W1n93Ka3gYN8CiiOP08DJkL4z8Mhtmrprxlx__Ow7AjUnrI";
//var Future = Meteor.require('future');
Future = Npm.require('fibers/future');

Meteor.methods({
    'stackBasic':function(){
        var futt=new Future();
        var userDetailsURL = "http://api.stackexchange.com/2.2/users/3248794";
          HTTP.call("GET", userDetailsURL,{params:{site:"stackoverflow",key:'0DIk0wZkEh9G1uo*R3fvNg(('}},function(error,result)
          {
                futt.return(result);
                console.log(result);
           }); 
           return futt.wait();
    },
    'saveStackData':function(id){
        console.log(id);
        Meteor.users.update({_id:this.userId},{$set:{"profile.stackId":id}});
    },
    'linkedInapi':function(){
        var fut=new Future();
        // var accesstoken=Meteor.user().services.linkedin.accessToken;
        var url="https://api.linkedin.com/v1/people/~:(first-name,last-name,location,headline,num-connections,summary,picture-url,positions,following,skills,num-recommenders,connections:(first-name,last-name,picture-url,positions,public-profile-url))";
        HTTP.call("GET",url,{params:{oauth2_access_token:azadtoken,format:"json"}},function(e,res){            
            fut.return(res); 
            
        });
        return fut.wait();
    },
    'linkedinCompany':function(compId)
    {
        var futu=new Future();
        var url="https://api.linkedin.com/v1/companies/"+compId+":(id,name,logo-url,industries)";
        HTTP.call("GET",url,{params:{oauth2_access_token:azadtoken,format:"json"}},function(e,res){            
            futu.return(res);           
        });
        return futu.wait();
    },
    'linkedInapiConnections':function(){
         var futy=new Future();
        // var accesstoken=Meteor.user().services.linkedin.accessToken;
        var url="https://api.linkedin.com/v1/people/~/connections:(positions)";
        HTTP.call("GET",url,{params:{oauth2_access_token:azadtoken,format:"json"}},function(e,res){            
            futy.return(res);           
        });
        return futy.wait();
    },
    'githubBasicDetails':function(uname,upass){
       
        github.authenticate({
            type: "basic",
            username: uname,
            password: upass
        });
        //Session.set("gitUName",uname);
       var gists = Async.runSync(function(done) {
        github.user.get({}, function(err, data) {
          done(null, data);
        });
      });
      
      return gists;
    },
   /* 'repocontent':function(uname,repoName){
        var reposcontent=Async.runSync(function(done){
           github.repos.getContent({user:uname,repo:repoName,path: ""},function(err,data){
              done(null,data) ;
           }); 
        });
        return reposcontent;
    },*/
    'userRepos':function(){
        var repos=Async.runSync(function(done){
           github.repos.getAll({},function(err,data){
              done(null,data) ;
           }); 
        });
        return repos;
    },
    'orgRepos':function(orgid){
        var orgRepos=Async.runSync(function(done){
           github.repos.getFromOrg({org:orgid},function(err,data){
              done(null,data) ;
           }); 
        });
        return orgRepos;
    },
    
    'userOrgs':function(){
        var orgs=Async.runSync(function(done){
           github.user.getOrgs({},function(err,data){
              done(null,data) ;
           }); 
        });
        return orgs;
    },
    
    'repoData':function(uname,reponame){
        var repoData=Async.runSync(function(done){
            github.repos.get({user:uname,repo:reponame},function(repoerr,repodata){                
                if(repoerr){
                    console.log(repoerr);
                    console.log("error")
                }
                done(null,repodata);
            });
        });
        return repoData;
       
    },
    
    'repoCommits':function(uname,reponame){
        
        var repocommits=Async.runSync(function(done){
            github.repos.getCommits({user:uname,repo:reponame,author:uname},function(err,data){
                done(null,data);
            });
        });
        
        return repocommits;
        
    },
    
    'stats':function(uname,reponame,sha){
        var stats=Async.runSync(function(done){
            github.repos.getCommit({user:uname,repo:reponame,sha:sha},function(err,data){
                done(null,data);
            });
        });
       return stats;
    },
    
    'contributors':function(uname,reponame){
        var urls="https://api.github.com/repos/"+uname+"/"+reponame+"/stats/contributors";
        var cont=Async.runSync(function(done){
            HTTP.call("GET",urls,{},function(e,r){
                done(null,r);
            });
             /*github.repos.getStatsContributors({user:uname,repo:reponame},function(err,data){
                done(null,data);
            });*/
        });
        return cont;
    },
    
    'repoLanguage':function(uname,reponame){
        var repoLan=Async.runSync(function(done){
            github.repos.getLanguages({user:uname,repo:reponame},function(err,data){
                done(null,data);
            });
        });
        return repoLan;
    },
    'gists':function(){
        var gists=Async.runSync(function(done){
            github.gists.getAll({},function(err,data){
                done(null,data);
            });
        });
        return gists;
    },
    'gist':function(id)
    {
        var gist=Async.runSync(function(done){
            github.gists.get({id:id},function(err,data){
                done(null,data);
            });
        });
        return gist;
    }
    
    
});

